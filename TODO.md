# user comments

> * let's change the font size of the text from yellow to white for every contact detail listed.
> * use the phone icon next to the phone number along with the text "Call Now"

> * show senators, not just representatives

> * test multiple instances of widget on the page
> * district overlay on map
    > * http://www.govtrack.us/congress/findyourreps.xpd

# user feedback

> * "Change Location" does not show up for me on the home page or most other pages. It did show up on one but not sure why. If I clicked on NC twice, I got a box to change location. The change works well when entering a request but need to have "Change Location" show up clearly on all pages.

> * Backing up did not work. Each time I saw a legislator's page if I tried to go back, the Back button did not work. I had to exit and begin again.

> * I'm never sure which district # is mine. My home page popped up with David Price. He is my rep. It would be good to add text to the top indicating, for example, "U.S. representative for that area. Click on state link at top for U.S. representatives for other areas in that state or state Senators".

> * Can you add a link that shows NC districts so it's possible to see where they are?  That should be readily available for voting.

> * On Home Page, enlarge "More Information" or change to "Instructions"
  Make More Info text content bold for old eyes
  Make More Info page red/white and blue?
  For a rep, change "form" to "contact form"
  More Info text should indicate that the links are to U.S. Representatives     and Senators in U.S. Congress to distinguish from state legislators.

> * Is it possible to also provide links to local reps in state legislature and NC officials, e.g. Governor, Attorney General, etc?  They are often the folks we need to contact for state and local issues. If not easy, go with what you have and think about possible addition if you see this will fly.

> * Do the links provided allow for both liberal and conservative views on the legislators' performance?

> * When pulling up YouTube, "Text Comments" open up with it. Some may be inappropriate or offensive to readers. Is there a simple way to allow reader to choose whether to view them?
