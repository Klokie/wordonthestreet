Word on the Street is a web widget that will automagically recognize the user's current location and display elected Congressional officials for that region (within the US).

Each Senator, Representative, or other delegate is displayed in an easy to read format including a recent photo, contact information, relevant links, and any recent posts they have made to their primary blogs or to Twitter, or videos they posted to YouTube. WOTS can be easily embedded into any website, blog, or social networking site (coming soon), and is specially suited for use on the iPhone, Google Android, BlackBerry, and other GPS-equipped devices as well as web browsers for Mac, Windows, and Linux, without requiring any additional software installation, customization, registration, or hosting. Since it is nearly effortless to use (at least I hope it is) and provides a lot of useful and timely information to the user, I am hoping that it will help to increase political transparency and public participation in US government.

Here's the basic flow:

1. As soon as the widget has rendered, WOTS should recognize the general area where you're located and pull up the list of all senators, representatives, and any other delegates to Congress for your area. If you are outside the US or Google Maps wasn't able to detect your location, you can select it by clicking on the map or typing it in to the "location" field.
2. You can change your location at any time by clicking the "change location" link at the top. You can try just about anything - city, state, zip code, intersection should all work (thanks to Google Maps)! A more specific location should provide you with a specific person to contact for that area, while a more general area or state name will bring up a list of legislators for the region.
3. The contact info and links sections are pretty concise, while the posts area is a lot more interesting. There is a wealth of information about all these people in a variety of places, but I'm trying to make it easier for citizens to quickly get up to date on exactly what their representatives are up to - and soon, what they are voting for and spending your tax money on - and to contact them if necessary.

Word on the Street may be embedded into any blog or website using the following three lines of HTML:

```html
<script type="text/javascript" src="http://www.google.com/jsapi?key=__GOOGLE_API_KEY__"></script>
<style type="text/css">@import "http://klokie.com/widgets/wots/css/style.css";</style>
<script type="text/javascript" src="http://klokie.com/widgets/wots/js/wots.js"></script>
```

A key is required to use the Google Maps API. Get one free from http://code.google.com/apis/maps/signup.html

Word on the Street is dual licensed under the GPL and MIT licenses.

I want to help get Americans more interested and involved in politics, so they can not only follow what their elected leaders are doing on a daily basis, but more importantly feel like they can voice their opinions to people in power and feel like their voices are getting heard. Finding out exactly who those powerful people are, starting with the legislators in US Congress, is the first step toward making positive change.

-- [Klokie](http://klokie.com/)