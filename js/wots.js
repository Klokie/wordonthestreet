/*!
* Word on the Street
*
* description
*   - A widget to display all of the elected Congressional officials for the user's current location (within the US)
*
* Copyright (c) 2009 Klokie <klokie@klokie.com>
*
* Version 1.1.0
*
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*
* Usage:
*   include the following lines to embed the widget in your html or blog code:
*
*   <script src="http://www.google.com/jsapi?key=__GOOGLE_KEY__" type="text/javascript"></script>
*   <style type="text/css">@import "http://klokie.com/widgets/wots/css/style.css";</style>
*   <script type="text/javascript" src="http://klokie.com/widgets/wots/js/wots.js"></script>
*
* Changes:
*   now sorting all posts with most recent at the top (thanks, tsort!)
*   geoCoding more defensively 
*   caching some of the results at the district level
*/
/**********************************************
* GLOBAL OBJECTS
***********************************************/
if (typeof WOTS === 'undefined')
 WOTS = {
    // options
    options: {
        debug: false,
        show_fullinfo: false,
        show_map: false,
        show_header: true,
        show_credits: true,
        services: {
            'google': {
                'base': '',
                'key': 'ABQIAAAARpMVjaUh24C1qb_H6jb_oxQacSPYSywOjMCrUDdczBKrBkgRfhRO05YZaSeaTDfoVJJblcUHGv0ocA'
            },
            'sunlightlabs': {
                'base': 'http://services.sunlightlabs.com/api/',
                'key': '782d642805e84979a3d8a5d7ca87c3ff',
                'methods': {
                    'getDistrict': 'districts.getDistrictFromLatLong.json?latitude={LAT}&longitude={LONG}&apikey={KEY}'
                    ,
                    'getLegislators': 'legislators.getList.json?state={state}&district={district}&apikey={KEY}'
                }
            }
            ,
            'opensecrets': {
                'base': 'http://www.opensecrets.org/api/?',
                'key': 'fa1b3fd43e230da643cf944b7bdced1b'
            }
            ,
            'images': {
                'base': 'http://www.opensecrets.org/politicians/img/pix/'
            }
            ,
            'topwords': {
                'base': 'http://capitolwords.org/api/',
                'methods': {
                    'top5': 'lawmaker/{bioguide_id}/latest/top5.json'
                }
            }
        }
    },
    // end options
    tmpl: {
        profileOpenHTML: '<div class="person hcard party-{party} status-{in_office} state-{state}">'
        ,
        profileCloseHTML: '</div>'
        ,
        profileHTML: {
            'contact': {
                'webform': '<span class="separator">&nbsp;✭&nbsp;</span><label class="online" for="www-{crp_id}">Email form:</label> '
                + '<a href="{webform}" target="_blank" class="webform" id="www-{crp_id}" title="URL of web contact form">webform</a>',
                'phone': '<label">tel:</label> '
                + '<a href="callto:+1{phone}" class="tel" id="tel-{crp_id}" title="Congressional office phone number - click to call (where available)">{phone}</a>'
                ,
                'fax': '<label">fax:</label> '
                + '<a class="fax" id="fax-{crp_id}" title="Congressional office fax number">{fax}</a>'
                ,
                'email': '<label">email:</label> '
                + '<a href="mailto:{email}" class="email" id="email-{crp_id}" title="email address">{email}</a>'
                ,
                'twitter_id': '<label>Twitter:</label> '
                + '<a href="https://twitter.com//#!/{twitter_id}" target="_blank" class="twitter_id" title="@{twitter_id} on Twitter.com">@{twitter_id}</a>'
            }
            ,
            'links': {
                'website': '<a href="{website}" class="url website" target="_blank" title="URL of Congressional website">{website}</a>'
                ,
                'twitter_id': '<a href="https://twitter.com//#!/{twitter_id}" target="_blank" class="twitter_id" title="Congressperson\'s official Twitter account">Twitter user &ldquo;{twitter_id}&rdquo;</a>'
                //,'youtube_url' : '<a href="{youtube_url}" class="youtube_url" title="Congressperson\'s official Youtube account">YouTube</a>'
                ,
                'congresspedia_url': '<a target="_blank" href="{congresspedia_url}" class="congresspedia_url" title="URL of entry in Congresspedia">Congresspedia</a>'
                ,
                'bioguide_id': '<a target="_blank" href="http://bioguide.congress.gov/scripts/biodisplay.pl?index={bioguide_id}" class="bioguide_url" title="URL of entry in the Congressional Biographical Directory">Congressional Biographical Directory</a>'
                ,
                'votesmart_id': '<a target="_blank" href="http://votesmart.org/bio.php?can_id={votesmart_id}" class="votesmart_url" title="URL of entry in the Project Vote Smart">Project Vote Smart</a>'
                ,
                'govtrack_id': '<a target="_blank" href="http://www.govtrack.us/congress/person.xpd?id={govtrack_id}" class="govtrack_url" title="URL of entry on Govtrack.us">Govtrack.us</a>'
                ,
                'crp_id': '<a target="_blank" href="http://www.opensecrets.org/politicians/summary.php?cid={crp_id}" class="crp_url" title="URL of entry on the Center for Responsive Politics">Center for Responsive Politics</a>'
                //,'eventful_id' : '<a target="_blank" href="http://eventful.com/performers/{eventful_id}" class="eventful_url" title="URL of entry on the Eventful.com">Eventful.com</a>'
            }
        },
        // end profileHTML
        header: '<div class="WOTSheader"><div class="info"><h1>Word on the Street</h1>'
        + '<h5 class="address">wordonthestreet.klokie.com</h5>'
        + '<blockquote class="description"><p class="strong"><em>Word on the Street</em> is a software widget that will automagically recognize the user\'s current location and display all of the elected Congressional officials for that region (within the US).</p>'
        + '<p>Each Senator, Representative, or other delegate is displayed in an easy to read format including a recent photo, contact information, relevant links, and any recent posts they have made to their primary blogs or to Twitter, or videos they posted to YouTube. WOTS can be easily embedded into any website, blog, or social networking site (coming soon), and is specially suited for use on the iPhone, Google Android, BlackBerry, and other GPS-equipped devices as well as web browsers for Mac, Windows, and Linux, without requiring any additional software installation, customization, registration, or hosting. Since it is nearly effortless to use (at least I hope it is) and provides a lot of useful and timely information to the user, I am hoping that it will help to increase political transparency and public participation in US government.</p><p>Here\'s the basic flow:'
        + '<ol><li>You arrive here. WOTS should recognize the general area where you\'re located and pull up the list of all senators, representatives, and any other delegates to Congress for your area. If you are outside the US or Google Maps wasn\'t able to detect your location, you can select it by clicking on the map or typing it in to the "location" field.</li>'
        + '<li>You can try just about any U.S. address - city, state, zip code, intersection should all work (thanks to Google Maps)! A more specific location should provide you with a specific person to contact for that area, while a more general area or state name will bring up a list of legislators for the region.</li>'
        + '<li>The contact info and links sections are pretty concise, while the posts area is a lot more interesting. There is a wealth of information about all these people in a variety of places, but I\'m trying to make it easier for citizens to quickly get up to date on exactly what their representatives are up to - and soon, what they are voting for and spending your tax money on - and to contact them if necessary.</li></ol></p>'
        + '<p>I want to help get Americans more interested and involved in politics, so they can not only follow what their elected leaders are doing on a daily basis, but more importantly feel like they can voice their opinions to people in power and feel like their voices are getting heard. Finding out exactly who those powerful people are, starting with the legislators in US Congress, is the first step toward making positive change.</p>'
        + '<p>If you have any comments or questions, please feel free to email me at <a href="mailto:%20wots@klokie.com">wots@klokie.com</a>.</p>'
        + 'Thanks!<div class="created">3/28/2009</div><a href="mailto:%20wots@klokie.com" class="author">Klokie</a></blockquote>'
        + '</div>'
        ///.info
        + '<div class="menu">'
        //+ '<a href="#WOTSmap" class="WOTSCL" class="tab map">Change Location</a>'
        + '<a href="#WOTSabout" class="tab about">Learn More</a></div>'
        + '</div>'
        ///#header
        ,

        controls: '<div class="WOTScontrols">'
        + '<form method="GET" action="#" name="fControls" class="WOTSfControls">'
        + '<label for="q">'
        + "Enter full address if this is not your current location."
        + '</label> '
        + '<input type="text" class="WOTSaddressInput" name="q" id="q" value="" size="32" maxlength="50"/>'
        + '<input type="submit" class="submit" value="Go"/>'
        + '</form>'
        + '</div>'
        + '<div class="WOTSabout"></div>'
        + '<div class="WOTSresults"></div>'
        ,

        credits: '<div class="info hcard nowrap"><a href="http://klokie.com/" title="by Klokie">Klokie</a>'
        + ' | <a href="http://s2ai.net/main.php" class="url fn org">S2A Interactive</a> |'
        + '<span class="email"><a href="mailto:%20wots@klokie.com" class="value">wots@klokie.com</a></span>'
        + '</div>'
        ///.info
        + '<div class="nowrap">Flag photo courtesy of <a rel="license" href="http://www.flickr.com/photos/branditressler/" target="_BLANK">Brandi Tressler</a></div>'
        ,
        license: '<div class="license">'
        + '<div class="nowrap">Source: <a href="http://sunlightfoundation.com/" target="_BLANK">Sunlight Foundation</a></div></div>',
        loading_address: '<strong class="progress">Determining address&hellip;</strong>',
        getting_location: '<strong class="progress">Determining selected location&hellip;</strong>',
        bad_address: "Unable to find address",
        invalid_address: '<strong>Please enter a valid combination of city, state, and/or zip.</strong>',
        bad_browser: '<strong>Sorry, your browser is not supported at this time.</strong>'
    },
    // end tmpl
    // google map data cache
    currentLocation: {
        'lat': null,
        'lng': null,
        'state': null,
        'country': null,
        'district': null
    },
    geocoder: {},
    cache: {},
    GMap: null,
    // library dependencies
    dependencies: {
        'feeds': ['1'],
        'maps': ['2', {
            'other_params': 'sensor=true'
        }]
    },
    // jquery noconflict
    $: {},
    /**
        * message logging function
        */
    log: function() {
        var args = WOTS.log.arguments;
        if (WOTS.options.debug && typeof console != 'undefined') console.log(args);
        if (typeof WOTS.$ == 'function') {
            var msg = WOTS.serialize(WOTS.$.makeArray(args));
            WOTS.$('.WOTSabout').html(msg);
        }
    },

    /**
    * helper function to count object properties
    */
    getLength: function(obj) {
        var len = 0;
        for (var x in obj) if (obj.hasOwnProperty(x)) len++;
        return len;
    },

    serialize: function(obj) {
        var out = '';
        if (typeof obj === 'string') {
            out += obj;
        } else {
            for (var i in obj) {
                if (out.length) out += '\n';
                if (i == 0) out += WOTS.serialize(obj[i]);
                else out += i + ': ' + WOTS.serialize(obj[i]);
            }
        }
        return out;
    },
    // end serialize
    /**
            * display the google map
            */
    map: function(point) {
        if (WOTS.GMap && !WOTS.GMap.isLoaded()) {
            if (!point) {
                // display the map and center to current location
                point = WOTS.getCurrentLocation();
            }
            var customUI = WOTS.GMap.getDefaultUI();
            customUI.controls.menumaptypecontrol = false;
            WOTS.GMap.setUI(customUI);
            if (point) WOTS.GMap.setCenter(point, 10);
        }

        if (point) WOTS.showSelection(point);

        if (WOTS.options.show_header) {
            WOTS.$('.WOTSheader .menu .map').show();
        }
        if (WOTS.options.show_map) {
            WOTS.$('.WOTSmap:hidden,.WOTScontrols:hidden').show();
        } else {
            WOTS.$('.WOTScontrols:hidden').show();
        }
    },
    // end map
    /**
            * display the "about" section
            */
    about: function() {
        if (WOTS.$('.WOTSabout .description').slideToggle().length == 0) {
            WOTS.$('.WOTSheader .info .description').clone().prependTo('.WOTSabout').show();
        }
    },

    /**
            * loadFromHistory
            * get history entries from #hash links and query string
            * @param hash String
            */
    loadFromHistory: function(hash) {
        var q = location.search.match(/q=([^&\?=]+)/),
        params = hash ? hash.replace(/^WOTS/, '').split(/:/) : [];

        if (params.length > 0 && typeof WOTS[params[0]] == 'function') {
            // restore ajax loaded state
            if (params.length > 2) WOTS[params[0]](params[2], params[1]);
            else WOTS[params[0]](params[1]);
            return;

        } else if (location.search && q) {
            WOTS.log('looking for: ' + unescape(q[1]));
            WOTS.findAddress(unescape(q[1]));

        } else {
            WOTS.log('<strong class="progress">Determining current location&hellip;</strong>');

            // display the map and center to current location
            WOTS.map(WOTS.getCurrentLocation());
        }

        if (WOTS.currentLocation.state) {
            WOTS.renderLegislatorsForLocation(WOTS.currentLocation);
        }
    },
    // end loadFromHistory
    /**
            *
            */
    getCurrentLocation: function() {
        var point = {};
        if (google.loader && google.loader.ClientLocation) {
            var lat = google.loader.ClientLocation.latitude,
            lng = google.loader.ClientLocation.longitude;
            point = new google.maps.LatLng(lat, lng);
        } else {
            point = new google.maps.LatLng(38.898648, 77.037692);
            // default location: The White House
        }
        return point;
    },

    /**
            * main
            */
    main: function(id) {
        WOTS.$(document).ready(function() {
            try {
                var $widget = WOTS.render(id);

                WOTS.log('Initializing&hellip;');

                if ($widget.length != 1) throw ("Can't find the WOTS widget.");

                WOTS.initGoogleMaps($widget);

                WOTS.$.history.init(WOTS.loadFromHistory);

            } catch(e) {
                WOTS.log('error: ', e);
            }
        });
    },
    // end main
    initLibraries: function() {
        /*!
        * jQuery TinySort
        */
        (function(e) {
            var a = false,
            g = null,
            f = parseFloat,
            b = /(\d+\.?\d*)$/g;
            e.tinysort = {
                id: "TinySort",
                version: "1.2.18",
                copyright: "Copyright (c) 2008-2012 Ron Valstar",
                uri: "http://tinysort.sjeiti.com/",
                licenced: {
                    MIT: "http://www.opensource.org/licenses/mit-license.php",
                    GPL: "http://www.gnu.org/licenses/gpl.html"
                },
                defaults: {
                    order: "asc",
                    attr: g,
                    data: g,
                    useVal: a,
                    place: "start",
                    returns: a,
                    cases: a,
                    forceStrings: a,
                    sortFunction: g
                }
            };
            e.fn.extend({
                tinysort: function(m, h) {
                    if (m && typeof(m) != "string") {
                        h = m;
                        m = g
                    }
                    var n = e.extend({},
                    e.tinysort.defaults, h),
                    s,
                    B = this,
                    x = e(this).length,
                    C = {},
                    p = !(!m || m == ""),
                    q = !(n.attr === g || n.attr == ""),
                    w = n.data !== g,
                    j = p && m[0] == ":",
                    k = j ? B.filter(m) : B,
                    r = n.sortFunction,
                    v = n.order == "asc" ? 1: -1,
                    l = [];
                    if (!r) {
                        r = n.order == "rand" ?
                        function() {
                            return Math.random() < 0.5 ? 1: -1
                        }: function(F, E) {
                            var i = !n.cases ? d(F.s) : F.s,
                            K = !n.cases ? d(E.s) : E.s;
                            if (!n.forceStrings) {
                                var H = i.match(b),
                                G = K.match(b);
                                if (H && G) {
                                    var J = i.substr(0, i.length - H[0].length),
                                    I = K.substr(0, K.length - G[0].length);
                                    if (J == I) {
                                        i = f(H[0]);
                                        K = f(G[0])
                                    }
                                }
                            }
                            return v * (i < K ? -1: (i > K ? 1: 0))
                        }
                    }
                    B.each(function(G, H) {
                        var I = e(H),
                        E = p ? (j ? k.filter(H) : I.find(m)) : I,
                        J = w ? E.data(n.data) : (q ? E.attr(n.attr) : (n.useVal ? E.val() : E.text())),
                        F = I.parent();
                        if (!C[F]) {
                            C[F] = {
                                s: [],
                                n: []
                            }
                        }
                        if (E.length > 0) {
                            C[F].s.push({
                                s: J,
                                e: I,
                                n: G
                            })
                        } else {
                            C[F].n.push({
                                e: I,
                                n: G
                            })
                        }
                    });
                    for (s in C) {
                        C[s].s.sort(r)
                    }
                    for (s in C) {
                        var y = C[s],
                        A = [],
                        D = x,
                        u = [0, 0],
                        z;
                        switch (n.place) {
                        case "first":
                            e.each(y.s,
                            function(E, F) {
                                D = Math.min(D, F.n)
                            });
                            break;
                        case "org":
                            e.each(y.s,
                            function(E, F) {
                                A.push(F.n)
                            });
                            break;
                        case "end":
                            D = y.n.length;
                            break;
                        default:
                            D = 0
                        }
                        for (z = 0; z < x; z++) {
                            var o = c(A, z) ? !a: z >= D && z < D + y.s.length,
                            t = (o ? y.s: y.n)[u[o ? 0: 1]].e;
                            t.parent().append(t);
                            if (o || !n.returns) {
                                l.push(t.get(0))
                            }
                            u[o ? 0: 1]++
                        }
                    }
                    return B.pushStack(l)
                }
            });
            function d(h) {
                return h && h.toLowerCase ? h.toLowerCase() : h
            }
            function c(j, m) {
                for (var k = 0, h = j.length; k < h; k++) {
                    if (j[k] == m) {
                        return ! a
                    }
                }
                return a
            }
            e.fn.TinySort = e.fn.Tinysort = e.fn.tsort = e.fn.tinysort
        })(jQuery);
        // set tsort global defaults:
        $.tinysort.defaults.order = "desc";
        $.tinysort.defaults.attr = "id";

        /*!
         * jQuery history plugin
         * 
         * The MIT License
         * 
         * Copyright (c) 2006-2009 Taku Sano (Mikage Sawatari)
         * Copyright (c) 2010 Takayuki Miwa
         * 
         * Permission is hereby granted, free of charge, to any person obtaining a copy
         * of this software and associated documentation files (the "Software"), to deal
         * in the Software without restriction, including without limitation the rights
         * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
         * copies of the Software, and to permit persons to whom the Software is
         * furnished to do so, subject to the following conditions:
         * 
         * The above copyright notice and this permission notice shall be included in
         * all copies or substantial portions of the Software.
         * 
         * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
         * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
         * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
         * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
         * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
         * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
         * THE SOFTWARE.
         */

        (function($) {
            var locationWrapper = {
                put: function(hash, win) {
                    (win || window).location.hash = this.encoder(hash);
                },
                get: function(win) {
                    var hash = ((win || window).location.hash).replace(/^#/, '');
                    try {
                        return $.browser.mozilla ? hash: decodeURIComponent(hash);
                    }
                    catch(error) {
                        return hash;
                    }
                },
                encoder: encodeURIComponent
            };

            var iframeWrapper = {
                id: "__jQuery_history",
                init: function() {
                    var html = '<iframe id="' + this.id + '" style="display:none" src="javascript:false;" />';
                    $("body").prepend(html);
                    return this;
                },
                _document: function() {
                    return $("#" + this.id)[0].contentWindow.document;
                },
                put: function(hash) {
                    var doc = this._document();
                    doc.open();
                    doc.close();
                    locationWrapper.put(hash, doc);
                },
                get: function() {
                    return locationWrapper.get(this._document());
                }
            };

            function initObjects(options) {
                options = $.extend({
                    unescape: false
                },
                options || {});

                locationWrapper.encoder = encoder(options.unescape);

                function encoder(unescape_) {
                    if (unescape_ === true) {
                        return function(hash) {
                            return hash;
                        };
                    }
                    if (typeof unescape_ == "string" &&
                    (unescape_ = partialDecoder(unescape_.split("")))
                    || typeof unescape_ == "function") {
                        return function(hash) {
                            return unescape_(encodeURIComponent(hash));
                        };
                    }
                    return encodeURIComponent;
                }

                function partialDecoder(chars) {
                    var re = new RegExp($.map(chars, encodeURIComponent).join("|"), "ig");
                    return function(enc) {
                        return enc.replace(re, decodeURIComponent);
                    };
                }
            }

            var implementations = {};

            implementations.base = {
                callback: undefined,
                type: undefined,

                check: function() {},
                load: function(hash) {},
                init: function(callback, options) {
                    initObjects(options);
                    self.callback = callback;
                    self._options = options;
                    self._init();
                },

                _init: function() {},
                _options: {}
            };

            implementations.timer = {
                _appState: undefined,
                _init: function() {
                    var current_hash = locationWrapper.get();
                    self._appState = current_hash;
                    self.callback(current_hash);
                    setInterval(self.check, 100);
                },
                check: function() {
                    var current_hash = locationWrapper.get();
                    if (current_hash != self._appState) {
                        self._appState = current_hash;
                        self.callback(current_hash);
                    }
                },
                load: function(hash) {
                    if (hash != self._appState) {
                        locationWrapper.put(hash);
                        self._appState = hash;
                        self.callback(hash);
                    }
                }
            };

            implementations.iframeTimer = {
                _appState: undefined,
                _init: function() {
                    var current_hash = locationWrapper.get();
                    self._appState = current_hash;
                    iframeWrapper.init().put(current_hash);
                    self.callback(current_hash);
                    setInterval(self.check, 100);
                },
                check: function() {
                    var iframe_hash = iframeWrapper.get(),
                    location_hash = locationWrapper.get();

                    if (location_hash != iframe_hash) {
                        if (location_hash == self._appState) {
                            // user used Back or Forward button
                            self._appState = iframe_hash;
                            locationWrapper.put(iframe_hash);
                            self.callback(iframe_hash);
                        } else {
                            // user loaded new bookmark
                            self._appState = location_hash;
                            iframeWrapper.put(location_hash);
                            self.callback(location_hash);
                        }
                    }
                },
                load: function(hash) {
                    if (hash != self._appState) {
                        locationWrapper.put(hash);
                        iframeWrapper.put(hash);
                        self._appState = hash;
                        self.callback(hash);
                    }
                }
            };

            implementations.hashchangeEvent = {
                _init: function() {
                    self.callback(locationWrapper.get());
                    $(window).bind('hashchange', self.check);
                },
                check: function() {
                    self.callback(locationWrapper.get());
                },
                load: function(hash) {
                    locationWrapper.put(hash);
                }
            };

            var self = $.extend({},
            implementations.base);

            if ($.browser.msie && ($.browser.version < 8 || document.documentMode < 8)) {
                self.type = 'iframeTimer';
            } else if ("onhashchange" in window) {
                self.type = 'hashchangeEvent';
            } else {
                self.type = 'timer';
            }

            $.extend(self, implementations[self.type]);
            $.history = self;
        })(jQuery);
    },
    // end initLibraries
    /**
            * renderLegislatorsForDistrict
            * retrieves and displays a list of legislators for a given city/state
            */
    renderLegislatorsForDistrict: function(place) {
        WOTS.log('<strong class="progress">Loading districts for '
        + (place.city ? place.city + ', ' + place.state: place.state)
        + "&hellip;</strong>");

        WOTS.getDistrict(place,
        function(json1) {

            // district fetch complete
            if (json1 && json1.response && json1.response.districts && json1.response.districts.length) {
                place.state = json1.response.districts[0].district.state;
                place.district = json1.response.districts[0].district.number;
            } else {
                WOTS.log('The district you selected could not be found. Please try another.');
                return false;
            }

            WOTS.renderLegislatorsForLocation(place);
        });
    },
    // end renderLegislatorsForDistrict
    /**
    *
    */
    getLabel: function(place, districts) {
        var html = '<span class="nowrap"><a href="#WOTSmap" onclick="WOTS.$(\'.WOTSaddressInput\').focus();return false">';

        if (WOTS.currentLocation.city) {
            html += WOTS.currentLocation.city + ', ';
        }
        html += WOTS.currentLocation.state;
        html += '</a>';

        if (place && place.district) {
            html += ', district ' + place.district;
        } else if (districts > 1) {
            //html += districts + ' districts';
            }

        html += '</span>';

        return html;
    },

    /**
            *
            */
    displayRenderedLegislators: function(place, classes) {
        var summary = {
            'districts': [],
            'inactive': [],
            'titles': []
        },
        x = 0;

        // been there, done that - show previous results and return
        if (WOTS.$('.WOTSresults').children().hide().filter('.' + classes.join('.')).show().children().each(function() {
            // generate stats from prerendered content
            var title = WOTS.$('.person .meta .title', this).text();
            ++summary.districts;

            if (WOTS.$('.person', this).hasClass('status-0'))++summary.inactive;
            else if (typeof summary.titles[title] == 'undefined')
            summary.titles[title] = 1;
            else++summary.titles[title];

        }).length > 0) {
            html = WOTS.getLabel(place, summary.districts);
            html += ' has <span class="nowrap">';

            for (title in summary.titles) {
                if (x++>0) html += (summary.titles[title] == 2) ? ' and ': ', ';
                html += summary.titles[title]
                + ' '
                + (title == 'Sen' ? 'Senator': title == 'Rep' ? 'Representative': title == 'Del' ? 'Delegate': title)
                ;
                if (summary.titles[title] != 1) html += 's';
            }
            if (summary.inactive > 0) {
                if (summary.districts || summary.titles[title]) html += ' and ';
                html += summary.inactive + ' inactive legislators';
            }

            html += '.</span>';

            WOTS.log(html);
            return true;
        }

        return false;
    },
    // end displayRenderedLegislators
    /**
    *
    */
    renderLegislatorsCallback: function(json2, classes) {
        // people fetch complete
        if (! (json2 && json2.response && json2.response.legislators && json2.response.legislators.length)) {
            WOTS.$('.WOTSresults').append('<ul class="legislators ' + classes.join(' ') + '">'
            + '<li>No legislators were found for this district. Please try another.</li>'
            + '</ul>'
            );
        } else {
            WOTS.renderSummary(json2.response.legislators, classes);
        }
    },

    fetchExtraSources: function(person) {
        // load posts
        var sources = {
            'RSS': {
                format: 'rss',
                url: person.official_rss
            }
            ,
            'YouTube': {
                format: 'rss',
                url: person.youtube_url,
                pattern: /.+[\.\/]youtube.com\/(\w+)\/?.*/,
                replacement: 'http://gdata.youtube.com/feeds/base/users/$1/uploads?alt=rss&amp;v=2&amp;orderby=published'
            }
            ,
            'Twitter': {
                format: 'json',
                url: person.twitter_id,
                pattern: /(.+[\.\/]twitter.com\/)?(\w+)\/?.*/,
                replacement: 'http://search.twitter.com/search.json?q=from%3A$2'
            }
        };

        for (feed in sources) {
            var url = sources[feed].url;
            if (typeof url != 'undefined' && url != '') {
                if (typeof sources[feed].pattern != 'undefined' && typeof sources[feed].replacement != 'undefined') {
                    url = url.replace(sources[feed].pattern, sources[feed].replacement);
                }
                WOTS.getFeed(sources[feed].format, url, '#p' + index + ' .person .hfeed', feed);
            }
        }
        // end source loop
    },

    /**
    *
    */
    renderSummary: function(legislators, classes) {
        var html = '',
        person = {},
        index = 0,
        summary = {
            'districts': [],
            'inactive': [],
            'titles': []
        };

        // loop thru all people
        for (index in legislators) {
            person = legislators[index].legislator;

            html += '<li id="p' + index + '">' + WOTS.formatLegislators(person) + '</li>';

            if (WOTS.options.show_fullinfo) {
                WOTS.fetchExtraSources(person);

                // compile stats
                ++summary.districts;
                if (!person['in_office']) {
                    ++summary.inactive;
                } else if (typeof summary.titles[person['title']] == 'undefined') {
                    summary.titles[person['title']] = 1;
                } else {
                    ++summary.titles[person['title']];
                }
            }
        };

        // display the full result set with summary
        WOTS.$('.WOTSresults').append('<ul class="legislators ' + classes.join(' ') + '">' + html + '</ul>')
        .show()
        .find("a[rel*='nofollow']").attr('target', '_BLANK')
        // launch external links in separate windows
        ;

        WOTS.showLegislators(summary);
    },


    /**
    *
    */
    showLegislators: function(summary) {
        var x = 0,
        title = '',
        html = '';

        var label = WOTS.getLabel();
        WOTS.log('<strong>Showing legislators for ' + label + '</strong>');
    },

    /**
            * renderLegislatorsForLocation
            * fetches & formats a list of legislators for a given location
            */
    renderLegislatorsForLocation: function(place) {
        if (place.district) var classes = ['district-' + place.district];
        // , 'state-' + place.state ];
        else var classes = ['state-' + place.state];

        // check cache in page
        if (WOTS.displayRenderedLegislators(place, classes)) return;

        WOTS.log('<strong class="progress">Loading legislators for '
        + (place.city ? place.city + ', ' + place.state: place.state)
        + (place.district ? ", district " + place.district: '') + "&hellip;</strong>");

        WOTS.getLegislators(place,
        function(json) {
            WOTS.renderLegislatorsCallback(json, classes);
        });
    },
    // end renderLegislatorsForLocation
    /**
            * renderGoogleFeed
            * fetch the content via Google Feeds or $.ajax
            * create a callback function which depends on the variable "selector"
            * @param id jQuery selector
            */
    renderGoogleFeed: function(id) {
        return function(result) {
            if (!result.error) {
                var html = WOTS.formatGoogleEntries(result.feed.entries);
            } else {
                html = '<div class="error">No posts available'
                // we don't display the full error message to the user
                //+': '+result.error.message
                + '</div>';
            }

            // display the formatted posts or error message, only if there are new valid results
            if (!result.error || WOTS.$('.hentry,.error', id).length == 0) {
                WOTS.$(id).children('.progress,.error').fadeOut().end().append(html);
                WOTS.$(id).children().tsort();
            }
        }
    },
    // end renderGoogleFeed
    /**
    *
    */
    formatGoogleEntries: function(results) {
        var html = '';
        for (var i = 0, j = results.length; i < j; i++) {
            var entry = results[i],
            entryDate = new Date(entry['publishedDate']),
            published = '';
            if (isNaN(entryDate)) published = '';
            else published = (entryDate.getMonth() + 1) + '/' + entryDate.getDate() + '/' + entryDate.getFullYear();

            html += '<li class="hentry" id="' + entryDate.getTime() + '">'
            + '<a class="entry-title bookmark" target="_blank" href="' + entry["link"] + '" title="'
            + entry["title"] + '">'
            + entry["title"]
            + '</a>'
            + ' <cite class="entry-summary">' + entry["contentSnippet"] + '</cite>'
            + (published ? ' (<abbr class="published" title="' + entry['publishedDate'] + '">' + published + '</abbr>)': '')
            + '</li>'
            ;
        }

        return html;
    },

    /**
    *
    */
    formatTwitterEntries: function(results) {
        var html = '';
        for (var i = 0, j = results.length; i < j; i++) {
            var entry = results[i],
            entryDate = new Date(entry['created_at']),
            published = '';

            if (isNaN(entryDate)) published = '';
            else published = (entryDate.getMonth() + 1) + '/' + entryDate.getDate() + '/' + entryDate.getFullYear();

            html += '<li class="hentry" id="' + entryDate.getTime() + '">'
            + '<a class="entry-title bookmark" href="https://twitter.com//#!/' + entry['from_user'] + '/status/'
            + entry['id']
            + '" target="_blank" title="Tweet from ' + entry["from_user"] + '">'
            + entry["text"]
            + '</a>'
            + ' ('
            + (published ? '<abbr class="published" title="' + entry['publishedDate'] + '">' + published + '</abbr>': '')
            + ')'
            + '</li>'
            ;
        }

        return html;
    },

    /**
            * renderTwitterFeed
            * fetch via XHR
            * Twitter-specific rendering implementation
            * create a callback function which depends on the variable "selector"
            * @param id jQuery selector
            */
    renderTwitterFeed: function(id) {
        return function(result) {
            if (result.results) {
                var html = WOTS.formatTwitterEntries(result.results);
            } else {
                html = '<div class="error">No posts available'
                // we don't display the full error message to the user
                //+': '+result.error.message
                + '</div>';
            }

            // display the formatted posts or error message, only if there are new valid results
            if (!result.error || WOTS.$('.hentry,.error', id).length == 0) {
                WOTS.$(id).children('.progress,.error').fadeOut().end().append(html);
                WOTS.$(id).children().tsort();
            }
        }
    },
    // end renderTwitterFeed
    /**
            * getFeed
            */
    getFeed: function(format, url, selector, source) {
        if (format == 'rss' || format == 'atom') {
            // here we do the actual feed fetch
            var feed = new google.feeds.Feed(url);
            feed.load(WOTS.renderGoogleFeed(selector));
        } else {
            WOTS.$.ajax({
                url: url,
                success: WOTS.renderTwitterFeed(selector),
                dataType: 'jsonp',
                cache: true,
                ifModified: true
            });
        }
    },
    // end getFeed
    /**
            * convert HTML entities back to HTML
            * useful for e.g. Twitter posts
            */
    decodeEntities: function(str) {
        return unescape(str).replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&').replace(/&quot;/g, '"').replace(/\"{2,}/g, '"');
    },

    /**
            * getTopWords
            */
    getTopWords: function(person, callback) {
        if (!callback) callback = {};
        var url = WOTS.options.services['topwords']['base'] + WOTS.options.services['topwords']['methods']['top5'];
        url = url.replace(/\{bioguide_id\}/, person.bioguide_id);

        WOTS.$.ajax({
            url: url,
            success: callback,
            dataType: 'jsonp',
            jsonp: 'callback',
            cache: true,
            ifModified: true
        });
    },

    /**
            * getDistrict
            * retrieves the district number from a lat/long pair
            * @param place Object
            * @param callback function
            */
    getDistrict: function(place, callback) {
        if (!callback) callback = {};
        var url = WOTS.options.services['sunlightlabs']['base'] + WOTS.options.services['sunlightlabs']['methods']['getDistrict'];
        url = url.replace(/\{LAT\}/, place.lat)
        .replace(/\{LONG\}/, place.lng)
        .replace(/\{KEY\}/, WOTS.options.services['sunlightlabs']['key']);

        WOTS.$.ajax({
            url: url,
            success: callback,
            dataType: 'jsonp',
            jsonp: 'jsonp',
            cache: true,
            ifModified: true
        });
    },

    /**
            * getLegislators
            * retrieves a list of legislators for a given location
            * @param place Object
            * @param callback function
            */
    getLegislators: function(place, callback) {
        if (!callback) callback = {};
        var url = WOTS.options.services['sunlightlabs']['base'] + WOTS.options.services['sunlightlabs']['methods']['getLegislators'];
        url = url.replace(/\{state\}/, place.state).replace(/\{KEY\}/, WOTS.options.services['sunlightlabs']['key']);
        if (place.district) url = url.replace(/\{district\}/, place.district)
        else url = url.replace(/&district=\{district\}/, '')

        WOTS.$.ajax({
            url: url,
            success: callback,
            dataType: 'jsonp',
            jsonp: 'jsonp',
            cache: true,
            ifModified: true
        });
    },

    /**
    * format meta info about the legislator
    */
    formatSingleLegislator: function(record) {
        return '<div class="meta">'
        + '<strong class="state" title="state">' + record['state'] + '</strong>'
        + '<strong class="district" title="If legislator is a representative, their district">' + (record['district'] >= 0 ? 'District ' + record['district'] : record['district']) + '</strong>'
        + '&nbsp;✭&nbsp;'
        + '<div title="Title held by this legislator, either Senator or Representative" class="title">'
        + (record['title'] == 'Sen' ? 'Senator': record['title'] == 'Rep' ? 'Representative': record['title'] == 'Del' ? 'Delegate': record['title']) + '</div>'
        + '<h5><a class="fn" href="{website}" target="_blank">'
        + '<span title="first name" class="firstname">' + record['firstname'] + '</span> '
        + '<span title="middle name or initial" class="middlename">' + record['middlename'] + '</span> '
        + '<span title="last name" class="lastname">' + record['lastname'] + '</span> '
        + '<div title="suffix (Jr., III, etc.)" class="name_suffix">' + record['name_suffix'] + '</div> '
        //,'nickname' : "Preferred nickname of legislator (if any)"
        + '</a></h5>'
        // end of .fn
        + '</div>'
        ;
    },

    /**
    * format extra meta info about the legislator
    */
    formatSingleLegislatorExtra: function(record) {
        html = ''
        + '<div class="party" title="Party affiliation">' + (record['party'] == 'D' ? 'Democrat': record['party'] == 'R' ? 'Republican': record['party']) + '</div> '
        + '<div class="bioguide_id">' + record['bioguide_id'] + '</div>'
        + '<div class="votesmart_id">' + record['votesmart_id'] + '</div>'
        + '<div class="fec_id">' + record['fec_id'] + '</div>'
        + '<div class="govtrack_id">' + record['govtrack_id'] + '</div>'
        + '<div class="crp_id">' + record['crp_id'] + '</div>'
        + '<div class="eventful_id">' + record['eventful_id'] + '</div>'
        + '<div class="official_rss">' + record['official_rss'] + '</div>'
        + '<div class="congress_office">' + record['congress_office'] + '</div>'
        ;

        // include an empty hfeed container for posts
        if (record['official_rss'] || record['youtube_url'] || record['twitter_id']) {
            html += '<fieldset class="links"><legend>Recent Posts</legend>'
            + '<ul class="hfeed">'
            + '<strong class="progress">Loading&hellip;</strong>'
            + '</ul></fieldset>'
            ;
        }

        return '<div class="meta">'
        + ' ✭ '
        + html
        + '</div>'
        ;
    },

    /**
            * formatLegislators
            */
    formatLegislators: function(record) {
        var html = '',
        line = '',
        heading = '',
        field = '',
        photo = ''
        ;

        // generate the HTML for this contact record
        if (!WOTS.options.show_fullinfo) {
            html += WOTS.formatSingleLegislator(record);
            html += WOTS.formatContactInfo('contact', record);
        } else {
            if (record['crp_id']) photo = '<img src="http://www.opensecrets.org/politicians/img/pix/' + record['crp_id'] + '.jpg"/>'
            else if (record['govtrack_id']) photo = '<img src="http://www.opencongress.org/images/photos/thumbs_125/' + record['govtrack_id'] + '.jpeg"/>';
            else if (record['votesmart_id']) photo = '<img src="http://watchdog.net/data/crawl/votesmart/photos/' + record['votesmart_id'] + '.JPG"/>'

            if (photo) {
                html += '<div class="photo">'
                + '<a href="{website}" class="url website" target="_blank" title="{title} {firstname} {middlename} {lastname} {suffix}">' + photo + '</a>'
                + '</div>'
                ;
            }

            html += WOTS.formatSingleLegislator(record);

            for (heading in WOTS.tmpl.profileHTML) {
                html += WOTS.formatContactInfo(heading, record);
            }

            html += WOTS.formatSingleLegislatorExtra(record);
        }

        return WOTS.template(WOTS.tmpl.profileOpenHTML + html + WOTS.tmpl.profileCloseHTML, record);
    },
    // end formatLegislators
    formatContactInfo: function(heading, record) {
        var line = '',
        html = '';
        for (field in WOTS.tmpl.profileHTML[heading]) {
            if (typeof record[field] != 'undefined' && record[field] != '') {
                line += '<div class="' + field + '">' + WOTS.tmpl.profileHTML[heading][field] + '</div>';
            }
        }
        if (line != '') {
            if (heading == 'contact') html += '<div class="info">';
            // wrapper div
            html += '<fieldset class="' + heading + '"><legend>' + heading + '</legend>' + line + '</fieldset>';
            if (heading == 'contact') html += '</div>';
            // close wrapper div
        }

        return html;
    },
    /**
            * template
            * do key substitutions within the HTML using keys from the record
            * @param str String containing key references
            * @param record Object containing key/value pairs
            */
    template: function(str, record) {
        return str.replace(/\{(\w+)\}/g,
        function(t, key) {
            return typeof record[key] != 'undefined' ? record[key] : '';
        }
        );
    },

    /**
    *
    */
    goToPoint: function(point) {
        if (!point) {
            WOTS.log(WOTS.tmpl.bad_address);
        } else {
            WOTS.map(point);
        }
    },

    /**
        * findAddress()
        * GeoCode an address and display on the map
        * @param address String
        */
    findAddress: function(address) {
        if (WOTS.geocoder) {
            if (!address) {
                WOTS.log(WOTS.tmpl.invalid_address);
                return false;
            } else {
                WOTS.log(WOTS.tmpl.loading_address);
                WOTS.geocoder.getLatLng(address, WOTS.goToPoint);
            }
        }

        return true;
    },
    // end findAddress
    /**
    *
    */
    getAddress: function(place) {
        var address = '';

        if (place.AddressDetails.Country.AdministrativeArea) {
            switch (place.AddressDetails.Accuracy) {
            case 9:
                //Premise
            case 8:
                //Address
            case 7:
                //Intersection
            case 6:
                //Street
            case 5:
                //Post code
                if (place.AddressDetails.Country.AdministrativeArea.Locality) {
                    WOTS.currentLocation.zip = place.AddressDetails.Country.AdministrativeArea.Locality.PostalCode.PostalCodeNumber;
                    WOTS.currentLocation.city = place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName;
                }
            case 4:
                //Town
                if (!WOTS.currentLocation.city && place.AddressDetails.Country.AdministrativeArea.Locality) {
                    if (place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName)
                    WOTS.currentLocation.city = place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName;
                    else
                    WOTS.currentLocation.city = place.AddressDetails.Country.AdministrativeArea.Locality;
                }
                if (WOTS.currentLocation.city) {
                    address += WOTS.currentLocation.city + ', ';
                }
            case 3:
                //Sub-region
            case 2:
                //Region
                WOTS.currentLocation.state = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
                address += WOTS.currentLocation.state;
            case 1:
                //Country
                WOTS.currentLocation.country = place.AddressDetails.Country.CountryNameCode;
            }
        } else {
            WOTS.currentLocation.state = '';
            WOTS.currentLocation.country = place.AddressDetails.Country.CountryNameCode;
        }

        if (WOTS.currentLocation.zip) address += ' ' + WOTS.currentLocation.zip;

        //address += ' ' + WOTS.currentLocation.country;
        return address;
    },

    /**
    * renderAddressName
    * @param place
    */
    renderAddressName: function(place, point) {
        if (place.AddressDetails.Country.CountryNameCode == 'US') {
            //var point = new GLatLng(place.Point.coordinates[1],place.Point.coordinates[0]);
            var address,
            accuracy = place.AddressDetails.Accuracy;

            if (WOTS.GMap) {
                //if (!WOTS.GMap.getBounds().contains(point))
                WOTS.GMap.panTo(point);

                //WOTS.GMap.setZoom(12 - accuracy);
                //WOTS.GMap.setZoom(10);
            }

            WOTS.currentLocation = {
                'lat': point.lat(),
                'lng': point.lng()
            };

            address = WOTS.getAddress(place);

            if (WOTS.options.show_header) {
                WOTS.$('.WOTSheader .address').text(address);
            }
            WOTS.$('.WOTSaddressInput').val(address);

            if (WOTS.currentLocation.city) {
                WOTS.renderLegislatorsForDistrict(WOTS.currentLocation);
            } else {
                WOTS.renderLegislatorsForLocation(WOTS.currentLocation);
            }

            if (WOTS.GMap) {
                WOTS.GMap.savePosition();

                if (WOTS.addMarker) {
                    if (typeof marker != 'undefined') {
                        WOTS.GMap.removeOverlay(marker);
                    }

                    marker = new google.maps.Marker(point);
                    WOTS.GMap.addOverlay(marker);

                    marker.openInfoWindowHtml('<div>' + address + '</div>');
                    google.maps.Event.addListener(marker, 'click',
                    function() {
                        marker.openInfoWindowHtml("long = " + point.x, "lat = " + point.y);
                    });
                }
            }
        } else {
            // handle click point outside the US
            WOTS.log('Please select a location within the USA.');

            if (WOTS.options.show_map) {
                WOTS.$('.WOTSmap:hidden,.WOTScontrols:hidden').show();
            }
        }
    },
    // end renderAddressName
    /**
        * find and zoom in on a given address or point
        * @param point GLatLng
        */
    showSelection: function(point) {
        WOTS.geocoder.getLocations(point,
        function(response) {
            if (!response || response.Status.code != 200) {
                WOTS.log("Google Maps unavailable, please try again later (code:" + response.Status.code + ")");
            } else {
                place = response.Placemark[0];

                WOTS.renderAddressName(place, point);
            }
        });
    },
    // end showSelection
    /**
        * init the map object
        * attempts to get user's current location; default to GeoCode for US Congress
        */
    initGoogleMaps: function($widget) {
        if (GBrowserIsCompatible()) {
            try {
                var $map = WOTS.$(".WOTSmap", $widget),
                map = $map.get(0);
                if (WOTS.options.show_map) {
                    $map.show();
                }

                WOTS.geocoder = new google.maps.ClientGeocoder();
                WOTS.geocoder.setBaseCountryCode('US');

                if (WOTS.options.show_map) {
                    // set up standard UI without the map type selector
                    WOTS.GMap = new google.maps.Map2(map);

                    // process clicks on new locations on the map
                    var lstner = google.maps.Event.addListener(WOTS.GMap, 'click',
                    function(overlay, point) {
                        if (point) {
                            WOTS.log(WOTS.tmpl.getting_location);
                            var point = new google.maps.LatLng(point.y, point.x);
                            WOTS.goToPoint(point);
                        }
                    });
                }

                WOTS.$('.WOTSfControls', $widget).submit(function() {
                    location.href = '#WOTSfindAddress:' + encodeURIComponent(this.q.value);
                    return false;
                });
                WOTS.$('.WOTSaddressInput', $widget).focus();

                WOTS.$(window).unload(function() {
                    GUnload();
                });

            } catch(e) {
                throw (e);
            }
        } else {
            WOTS.log(WOTS.tmpl.bad_browser);
        }
    },
    // end initGoogleMaps
    /**
        * render the main app
        */
    render: function(id) {
        var html = '';

        if (WOTS.options.show_header) {
            html += WOTS.tmpl.header;
        }

        if (WOTS.options.show_map) {
            html += '<div class="WOTSmap"></div>';
        }

        html += WOTS.tmpl.controls;

        html += '<div class="WOTSfooter">';

        html += WOTS.tmpl.license;

        if (WOTS.options.show_credits) {
            html += WOTS.tmpl.credits;
        }

        html += '</div>'
        + '</div>'
        ;

        return WOTS.$('#' + id).html('<div id="' + id + '">' + html + '</div>');
    },
    // end render
    /**
        *
        */
    autoloader: function(src, callback) {
        if (typeof jQuery === 'undefined') {
            var head = document.getElementsByTagName("head")[0];
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = src;
            WOTS.loaded = false;

            script.onload = script.onreadystatechange = function() {
                if (!WOTS.loaded && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {

                    WOTS.loaded = true;
                    WOTS.log('loaded', src);

                    if (typeof callback === 'function') {
                        callback();
                    }

                    // Prevent IE memory leaking
                    //script.onload = script.onreadystatechange = null;
                    //head.removeChild(script);
                }
            };
        } else {
            }

        head.appendChild(script);
    },

    /**
        * called when a single dependency has loaded
        * @param lib machine name of library loaded
        * @param complete callback function when done
        */
    autoloadCallback: function(lib, complete) {
        try {
            delete WOTS.dependencies[lib];
            var len = WOTS.getLength(WOTS.dependencies);
            WOTS.log('autoloadCallback', lib, len + ' more to go');

        } catch(e) {
            WOTS.log('error', e);
        }

        if (len == 0) {
            complete();
        }
    },

    /**
    *
    */
    setupAutoloader: function(id, callback) {
        // first load Google AJAX loader
        WOTS.autoloader("https://www.google.com/jsapi",
        function() {
            // then load jQuery from Google -- cannot be inlined with google.load!
            WOTS.autoloader("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js",
            function() {
                WOTS.initLibraries();
                WOTS.$ = jQuery.noConflict();

                // number of dependencies to load
                var len = WOTS.getLength(WOTS.dependencies);

                if (len == 0) {
                    callback();
                    // nothing to do but continue
                } else {
                    var getParams = function(lib) {
                        var params = (typeof WOTS.dependencies[lib][1] === 'object') ? WOTS.dependencies[lib][1] : {};
                        params.callback = function() {
                            WOTS.autoloadCallback(lib, callback);
                        }
                        return params;
                    }
                    for (var lib in WOTS.dependencies) {
                        google.load(lib, WOTS.dependencies[lib][0], getParams(lib));
                    }
                }
            });
        });
    },

    /**
        * create a new container element, unique by timestamp
        * load and initialize dependencies
        */
    init: function() {
        var d = new Date(),
        id = 'P' + d.getTime();

        document.write('<div class="WOTS" id="' + id + '"></div>');

        WOTS.setupAutoloader(id,
        function() {
            WOTS.main(id);
        });

    }
    // end init
};
// end:WOTS
 (function() {
    WOTS.init();
})();